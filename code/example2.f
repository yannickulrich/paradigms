      PROGRAM HERON
      READ(5, *) IA, IB, IC
      IF (IA <= 0) STOP 1
      IF (IB <= 0) STOP 1
      IF (IC <= 0) STOP 1
      IF (IA+IB-IC <= 0) STOP 1
      IF (IA+IC-IB <= 0) STOP 1
      IF (IB+IC-IA <= 0) STOP 1
      S = FLOAT (IA + IB + IC) / 2.0
      AREA = SQRT( S * (S - FLOAT(IA)) * (S - FLOAT(IB)) *
     +     (S - FLOAT(IC)))
      WRITE (6, *) "area = ", AREA, "SQUARE UNITS"
      END PROGRAM
