import math as maths


class Shape:
    area = 0
    nsides = 0

class Triangle(Shape):
    a = 0
    b = 0
    c = 0

    def check(self):
        return (
            self.a > 0 and self.b > 0  and self.c > 0 and
            self.a + self.b - self.c > 0 and
            self.a + self.c - self.b > 0 and
            self.b + self.c - self.c > 0
        )

    def calc_size(self):
        assert self.check()
        self.area = maths.sqrt(s * (s-self.a) * (s-self.b) * (s-self.c))
