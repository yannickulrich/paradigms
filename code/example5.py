def square_and_sum_imp(lst):
    tally = 0
    for i in range(len(lst)):
        if lst[i] % 2 == 0:
            tally = tally + lst[i]**2

    return tally


def square_and_sum_decl(lst):
    return sum(i**2 for i in lst if i % 2 == 0)


from functools import reduce, partial
def compose(*func):
    def compose1(f, g):
        return lambda x : f(g(x))
    return reduce(compose1, func, lambda x : x)

evenQ = lambda x : x % 2 == 0
square1 = lambda x : x**2
tally = lambda x, y : x + y

square_and_sum_func = lambda lst : reduce(tally, map(square1, filter(evenQ, lst)))


filterEven = partial(filter, evenQ)
squareEach = partial(map, square1)
tallyAll = partial(reduce, tally)

square_and_sum = compose(tallyAll, squareEach, filterEven)
