#include <stdbool.h>
#include <math.h>
struct triangle_t {
    double a, b, c;
    double area;
};

bool check_triangle(struct triangle_t *self) {
    return self->a > 0 && self->b > 0  && self->c > 0 &&
            self->a + self->b - self->c > 0 &&
            self->a + self->c - self->b > 0 &&
            self->b + self->c - self->c > 0;
}

void calc_size(struct triangle_t *self) {
    double s = (self->a + self->b + self->c) / 2;
    self->area = sqrt(s * (s-self->a) * (s-self->b) * (s-self->c));
}

