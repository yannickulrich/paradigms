---
title: compiled vs. interpretted and other warnings
id: warnings
next: warnings2
---

 * some people make this distinction between compiled and interpreted languages and relate this to type systems

 * some people think languages are either object-orientated (C++, Python) or not (C, Fortran)

 * some people think types replace tests and vice versa
