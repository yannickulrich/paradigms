---
title: words
id: words
start: true
next: history1
---
<div>

There are a lot of buzzwords surrounding programming
 * compiled vs. interpreted
 * static vs. dynamic typing vs. duck typing
 * object-oriented vs. functional vs. imperative vs. declarative vs.
   structured vs. reflective

 * side effects, currying

what do they all mean??

but first, a brief history lesson
</div>
