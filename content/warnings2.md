---
title: all of that is nonsense
id: warnings2
next: wat
---

 * some people make this distinction between compiled and interpreted languages and relate this to type systems

    * you can build an interpreter for the most strongly typed languages (eg. Haskell) or a compiler for most flimsily typed ones (JavaScript)

    * (since x86 is far from the only target in the age of wasm, the distinction is more pointless)

 * some people think languages are either object-orientated (C++, Python) or not (C, Fortran)

    * you can write object oriented code in C and this is how Gnome works

 * some people think types replace tests and vice versa

    * no, they don't

As (software) engineers it's our job to pick the right tool for the right job.
That tool is hardly ever JavaScript
