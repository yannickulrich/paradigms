---
title: "\U0001f986 typing"
id: duck
layout: two-column
next: warnings
---

 * *duck typing* uses the *duck test* to determine compatibility

 * yes, these are the technical terms and no, I'm not kidding

 * used predominantly in dynamic & structurally contexts

```python
def check_if_duck(obj):
    try:
        obj.walk()
        obj.quack()
        return True
    except:
        return False
```

 * `Cows` and `Eagles` are *not* `Ducks`

---

```python
class Duck:
    def walk(self):
        print("waddle")
    def quack(self):
        print("quack")

class Cow:
    def walk(self):
        print("plod")
    def moo(self):
        print("mooh")

class Eagle:
    def fly(self):
        print("swoosh")
    def screech(self):
        print("screech")
```
