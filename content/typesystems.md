---
title: Type systems
id: typesystems
next: duck
---

 * strong vs. weak

    * not consistently defined

    * `1+"1"`. strong: fails, weak: *something* happens (coercion);
      `1+1.0` is more subtle

 * static vs. dynamic

    * types are known without running the code (usually at compile time) and can't change

    * C++, C, Haskell vs. Python, JavaScript

 * manifest vs. inferred

    * you manually declare the type vs. the compiler figures it out

    * C++, Rust vs. Python, Rust, Haskell

 * nominal vs. structural

    * compatibility is determined based on name (`int`) vs. structure (can add 1 to it)

    * C++ vs. Haskell, Python, Mathematica (to some extent)
