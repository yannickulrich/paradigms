---
title: Functional programming
id: functional
next: currying
---

<div>

 * we can make this more ... aesthetically pleasing

 * decompose our operation into single steps that do exactly one thing

    * filter the list

    * square the elements

    * sum the list

```python
evenQ = lambda x : x % 2 == 0
square1 = lambda x : x**2
tally = lambda x, y : x + y

square_and_sum = lambda lst : reduce(tally, map(square1, filter(evenQ, lst)))
```

 * is this better? who knows..

 * it's easier too debug though since `evenQ` just does ONE thing without *side effects*

</div>
