---
title: side note on referees
id: csreject
next: structureddata
---
<div>
When Djikstra submitted his paper, the referee had things to say

> Structured programming is a nice academic exercise, which works well for small examples, but I doubt that any real-world program will ever be written in such a style. [...]
> [Goto's] presence might cause some inconveniences in debugging, but it is a de facto standard and we must live with. [...]
> Publishing this would waste valuable paper: [...] I am confident that, 30 years from now, the goto will still be alive and well and used as widely as it is today.

This is the `while` loop and `if` statement they are talking about...

See [https://bit.ly/csreject](https://www.destroyallsoftware.com/misc/reject.pdf) for more
</div>
