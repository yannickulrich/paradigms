---
title: Currying
id: currying
next: typing
---

<div>

 * a function of the form $x=f(a,b,c)$ can always be written as $x=i(c)$, $i=h(b)$, $h=g(a)$ or $x=g(a)(b)(c)$

 * this is called *currying* or *partial application*

```python
times = lambda a, b : a * b
double = partial(times, 2)
```

 * why bother? the `square_and_sum` is still too complicated

```python
square_and_sum = lambda lst : reduce(tally, map(square1, filter(evenQ, lst)))
```

 * instead, let's over do things significantly
```python
filterEven = partial(filter, evenQ)
squareEach = partial(map, square1)
tallyAll = partial(reduce, tally)

square_and_sum = compose(tallyAll, squareEach, filterEven)
```

 * please, don't do this in python ... ever!

 * use Haskell instead

</div>
