---
title: wat -- javascript is crazy
id: wat
---
<div>

Q: why does any of this matter?

A: do this properly or people will make fun of you

```javascript
[] + [] == ""
[] + {} == {}
{} + [] == 0
{} + {} == NaN
Array(16).join("wat")
Array(16).join("wat"+1)
Array(16).join("wat"-1)
```

all taken from [Gary Bernhardt](https://www.destroyallsoftware.com/talks/wat)

</div>
