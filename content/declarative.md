---
title: Declarative programming
id: declarative
next: functional
---
<div>

 * let's declare the control flow implicitly rather then imperatively

```python
def square_and_sum(lst):
    return sum(i**2 for i in lst if i % 2 == 0)
```

 * didn't specify a loop, *barely* specified an `if`

 * this is probably closer to how you thought about the problem in the first place

 * pro: more concise, less syntactic sugar

 * con: more concise, less syntactic sugar

 * other examples: SQL queries for database, matplotlib for plotting rather than manually coding the pdf file

</div>
