---
title: a brief history lesson from the stone age
id: history1
next: goto
---
<div>
In Ye Olden Days, we wrote code like this

```fortran
      READ(5, 501) IA, IB, IC
  501 FORMAT (3I5)
      IF (IA) 777, 777, 701
  701 IF (IB) 777, 777, 702
  702 IF (IC) 777, 777, 703
  703 IF (IA+IB-IC) 777, 777, 704
  704 IF (IA+IC-IB) 777, 777, 705
  705 IF (IB+IC-IA) 777, 777, 799
  777 STOP 1
  799 S = FLOAT (IA + IB + IC) / 2.0
      AREA = SQRT( S * (S - FLOAT(IA)) * (S - FLOAT(IB)) *
     +     (S - FLOAT(IC)))
      WRITE (6, 601) IA, IB, IC, AREA
  601 FORMAT (4H A= ,I5,5H  B= ,I5,5H  C= ,I5,8H  AREA= ,F10.2,
     +        13H SQUARE UNITS)
      STOP
      END
```

£10 for anyone who can explain how and why this works and how to use
it right this minute
</div>
