---
title: Imperative programming
id: imperative
next: declarative
---
<div>

 * new example: given a list, square every even element, then sum them

```python
def square_and_sum(lst):
    tally = 0
    for i in range(len(lst)):
        if lst[i] % 2 == 0:
            tally = tally + lst[i]**2

    return tally
```

 * sure, this is nicer than the nightmare of 1950 but we still have to do all the coding

 * this was

     * *structured*: using loops rather than `goto` (try writing this in ancient Fortran&trade;)

     * *imperative*: we described every step for the computer to do

     * dynamically typed (see later)

</div>
