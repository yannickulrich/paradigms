---
title: Typing
id: typing
next: typesystems
---

 * type systems can protect you from yourself if used correctly

 * which no one does

 * turn runtime errors into compiler errors

 * different ways types works

 * error types can be used *instead* of exception and/or `NULL`

 * C++ doesn't have a good enough type system for this to work properly, Rust, Haskel, F# do

 * different ways to talk about type systems:

     * strong/weak, static/dynamic, manifest/inferred, nominal/structural, duck

 * see [here](https://www.destroyallsoftware.com/talks/ideology) for a talk of what types can and cannot do
