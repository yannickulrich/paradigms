---
title: Object Orientation
id: oop
next: imperative
---
<div>

 * objects tend be related, re-implementing everything all the time
   would be a nightmare..

 * solution: inheritance

 * two major school:

    * GObjectified-C: macros, macros everywhere!

    * C++: classes are first-class citizens

 * this usually also introduces exceptions

```python
class Shape:
    area = 0

class Triangle(Shape):
    def calc_size(self):
        self.area = ...

class Rectangle(Shape):
    def calc_size(self):
        self.area = ...
```
